# Changelog

## [Unreleased]

### Added

* ...

### Changed

* ...

### Fixed

* ...

### Removed

* ...

## [1.0.2] - 2018-08-22

### Added

* greetings and relation to upstream (Minimo)

### Changed

* new screenshot

### Removed

* unused i18n strings
* references to "authors" features of Minimo in opengraph

## [1.0.1] - 2018-08-22

### Fixed

* demo URL
* CHANGELOG errors

## [1.0.0] - 2018-08-22

* Initial stable release
* based on [Minimo](https://github.com/MunifTanjim/minimo) theme
* removed various widgets and blog functionalities

[unreleased]: https://framagit.org/severo/resumi/compare/1.0.1...master
[1.0.1]: https://framagit.org/severo/resumi/compare/1.0.0...1.0.1
