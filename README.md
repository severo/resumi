[![NPM release](https://img.shields.io/npm/v/resumi.svg?style=for-the-badge)](https://www.npmjs.com/package/resumi/v/latest)
[![license](https://img.shields.io/npm/l/resumi.svg?style=for-the-badge)](https://framagit.org/severo/resumi/blob/master/LICENSE)

![Resumi – resume theme for Hugo](https://framagit.org/severo/resumi/raw/master/images/tn.png)

# Resumi

Resume theme for Hugo, based on [Minimo](https://minimo.netlify.com/).

## Documentation

Check the [Resumi Documentation](./docs/README.md) for detailed documentation of
Resumi.

#### Getting Up & Running

Follow these guides for getting your site up & running with Resumi:

* **Install Resumi**:
  [Installation Guide](./docs/installation.md)
* **Configure Widgets**:
  [Widgets Documentation](./docs/widgets.md)

#### Updating Resumi

Follow the [**Updating Guide**](./docs/updating.md) to update Resumi to its
latest version.

After updating Resumi, always check that your site's **`config.toml`** file
matches the latest [**`config.toml`** file](./docs/config-file.md) format.

A good idea is to double check all the
[Configuration settings](./docs/installation.md#configuration-for-resumi) of
Resumi.

## Development

If you find a bug or want to request a new feature, feel free to open an issue.

Resumi is based on the [Minimo](https://minimo.netlify.com/) code by
[Munif Tanjim](https://github.com/MunifTanjim). Even if some of the blogging
features of Minimo have been removed from Resumi, we try to stick with upstream
and include the last commmits.

## Changelog

[Changelog for Resumi](https://framagit.org/severo/resumi/blob/master/CHANGELOG.md)

## License

Resumi is licensed under the MIT License. Check the
[LICENSE](https://framagit.org/severo/resumi/blob/master/LICENSE) file for
details.

The following resources are included/used in the theme:

* [Feather](https://feather.netlify.com/) by Cole Bemis - Licensed under the
  [MIT License](https://github.com/colebemis/feather/blob/master/LICENSE).

## What does Resumi even means?!

Resumi refers to its main goal: publish a resume.

Resumi is a reference to [Minimo](https://minimo.netlify.com/), the minimalist
theme it is based on.
