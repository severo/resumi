import './webpack-public-path'

import '../stylesheets/style'

import docReady from 'es6-docready'

docReady(() => {
  const body = document.body

  const detailsElements = body.querySelectorAll('details')
  if (detailsElements.length) {
    import(/* webpackChunkName: "details-polyfill" */ './details-polyfill').then(
      ({ detailsPolyfill }) => detailsPolyfill(detailsElements)
    )
  }

  let hasEmoji = body.classList.contains('has-emoji')
  if (hasEmoji) {
    let entry = body.querySelector('.entry')
    import(/* webpackChunkName: "twemoji" */ 'twemoji').then(twemoji =>
      twemoji.parse(entry)
    )
  }
})
