---
title: 'Escuela Militar de Ingeniería'
universalName: 'escuela-militar-de-ingeniería'
companyType: 'D'
industries:
  -
locations:
  - id: 1
    address:
      street1: 'Av. Arce No. 2642'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: '+591 2 2432266'
      phone2: '+591 2 2431641'
websiteUrl: 'http://www.emi.edu.bo'
logoUrl: ''
twitterId: ''
description: ''
isActive: true
---
