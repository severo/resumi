---
title: 'Jefe de unidad de innovación, investigación y desarrollo'
summary: ''
startDate: '2017-10-15'
endDate: '2018-02-28'
isCurrent: false
company 'agetic'
slug: 2017-agetic
languages:
  - es
languagesAlso:
  - en
---

Supervisión de proyectos de simplificación de trámites y gobierno electrónico,
asesoramiento técnico, redacción de normativa.
