---
title: 'Head of innovation, research and development unit'
summary: ''
startDate: '2017-10-15'
endDate: '2018-02-28'
isCurrent: false
company: 'adsib'
slug: 2015-adsib
languages:
  - es
languagesAlso:
  - en
skills:
  - project-management
  - technical-writing
  - policy-making
  - software-design
  - datacenter-design
skillsAlso:
  - linux-server-administration
  - networks-administration
---

Supervision of e-goverment projects, technical assessment, redaction of
technical norms.
