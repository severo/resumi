# Widgets

Resumi supports _**Widgets**_ that you can place on specific _**Widget Areas**_.

## Available Widgets

Resumi has the following built-in Widgets:

| Name           | Slug          |
| -------------- | ------------- |
| 1. About       | `about`       |
| 2. Search      | `search`      |
| 3. Social Menu | `social_menu` |

### Widget Configuration Options

Here are the configuration options available for the Widgets:

#### Widget: About

- `about` [`Map`]:
  - `title` [`String`]: Title  
     _default: `.Site.Title`_
  - `description` [`String`]: Description  
    _default: `.Site.Params.info.description`_
  - `logo` [`String`]: path/url of Logo  
    _default: `"/images/logo.png"`_

#### Widget: Search

- `search` [`Map`]:
  - `title` [`String`]: Title  
    _default: `"Search"`_

## Available Widget Areas

| Name     | Slug       |
| -------- | ---------- |
| Homepage | `homepage` |
| Footer   | `footer`   |

You can add Widgets to Widget Areas from your **`config.yml`** file:

```yaml
params:
  widgets:
    homepage:
      - 'about'
    footer:
      - 'search'
```

The syntax for adding Widgets to Widget Areas is:

```yaml
widget_area_slug:
  - widget_slug_1
  - widget_slug_2
  - ...
```

## Widgets Configuration File

Resumi picks up Widgets' configuration from the
[**`/data/config/widgets.toml`**](../data/config/widgets.toml) file.

If configuration for any Widgets are missing in this file, Resumi will fallback
to the default configuration for those Widgets.

Go wild!
