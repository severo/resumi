# Documentation

## Installation and configuration

- [Installation](./installation.md)
- [Updating](./updating.md)
- [Config file](./config-file.md)

## Basic features

- [Menus](./menus.md)
- [Shortcodes](./shortcodes.md)
- [Syntax highlighting](./syntax-highlighting.md)
- [Translation](./translation.md)

## Widgets

- [Cover image](./cover-image.md)
- [Emoji support](./emoji-support.md)
- [KaTeX support](./katex-support.md)
- [MathJax support](./mathjax-support.md)
- [OpenGraph](./opengraph.md)
- [Widgets](./widgets.md)

## Search

- [Search support](./search-support.md)
- [Algolia](./search-algolia.md)
- [Fuse.js](./search-fuse-js.md)
- [Lunr.js](./search-lunr-js.md)
