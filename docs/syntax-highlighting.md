# Syntax Highlighting

Hugo uses Chroma as it's built-in syntax-highlighter.

For detailed information about Syntax Highlighting in Hugo, check the
[Hugo's Syntax Highlighting Documentation](https://gohugo.io/content-management/syntax-highlighting/).
