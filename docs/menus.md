# Menus Setup Guide

Resumi has only one menu: Main Menu

## Main Menu

**name**: `main`

Main menu is located at the top of the site.

Check [Hugo's Menus Documentation](https://gohugo.io/content-management/menus/)
for information about managing this menu.

You can hide the Main Menu using the following option in your `config.yml`
file:

```yaml
params:
  settings:
    hideMainMenu: true
```

## Social Menu

Social Menu is available as Widget: `social_menu`. You can add it to [Widget
Areas](./widgets.md#available-widget-areas") to use it.

Use the variables under `params.social` in your `config.yml` file for adding
your social profiles:

```yaml
params:
  social:
    codepen: '...'
    email: '...'
    facebook: '...'
```

You only have to add your usernames. Minimo will take care of the rest.

#### Changing Social Menu Icons Order

If you want to change the order of the social menu icons, use the
`social_menu.platforms` option in your
[Widgets Configuration File](./widgets.md#widgets-configuration-file).

The social menu icons will appear in the order you specify in the `platforms`
array.
