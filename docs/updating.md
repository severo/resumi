# Updating Guide

## Updating Resumi

#### Update Resumi clone

Just replace the `themes/resumi` folder with the latest version of Resumi clone,
i.e. delete the old folder and clone again:

```sh
rm -rf themes/resumi
git clone --depth 1 https://framagit.org/severo/resumi.git
```

Then, commit the changes:

```sh
git add themes/resumi
git commit -m "update [theme]: resumi"
```

#### Update Resumi submodule

```sh
cd themes/resumi
git checkout master
git fetch && git pull
cd ../..
git add themes/resumi
git commit -m "update [theme]: resumi"
```

## To Do After Updating Resumi

After updating Resumi, always check that your site's **`config.yml`** file
matches the latest [**`config.yml`** file](../exampleSite/config.yml) format.

A good idea is to double check all the
[Configuration settings](./installation.md#configuration-for-resumi) of Resumi.
