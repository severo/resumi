# Opengraph Support

Resumi has built-in support for Opengraph tags. It will pick up most of the
things from your contents' front-matters & Hugo configuration.

You can use these options in you content's front-matter:

```yaml
audios: [] # for og:audio tags
images: [] # for og:image tags
videos: [] # for og:video tags
```

- `audios` [`Array` of `String`s]: path/url of audio files
- `images` [`Array` of `String`s]: path/url of image files
- `videos` [`Array` of `String`s]: path/url of video files

_N.B.: the front-matter syntax shown here is `yaml`. If you use a different
format (e.g. `toml`), the syntax will change accordingly._

If you add [**Cover Image**](./cover-image.md) to you content, it
will also be picked up.

## Facebook Opengraph

You can set these options in your `config.yml` file for better integration with
Facebook:

```yaml
params:
  opengraph:
    facebook:
      admins: [] # for fb:admins tags
      appID: '' # for fb:app_id tag
      pageID: '' # for article:publisher tag
```

- `admins` [`Array` of `String`s]: Facebook Profile IDs
- `appID` [`String`]: Facebook Application ID
- `pageid` [`String`]: Facebook Page ID

Also, the `social.facebook` field from the `config.yml` file data is used for
`article:author` tag.

## Twitter Cards

Resumi has built-in support for
[Twitter Cards](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards).

You can set these options in your `config.yml` files to provide additional
information for Twitter Cards:

```yaml
params:
  opengraph:
    twitter:
      page: '' # for twitter:site tag
```

- `page` [`String`]: Twitter Page's Username

Also, the `social.twitter` field from the `config.yml` file is used for
`twitter:creator` tag.
