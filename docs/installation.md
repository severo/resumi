# Installation Guide

First of all you will need to setup a Hugo site. You can follow the
[Hugo's Quick Start Guide](https://gohugo.io/getting-started/quick-start/) for
that.

After you're done with that, it's time for installing Resumi!

## Installing Resumi

There are two different ways you can install Resumi:

1.  As clone
2.  As submodule

_The second method is recommended._

#### Install Resumi as clone

With this method, you will simply clone it. And a copy of Resumi's repository
will be stored with the rest of you site. Enter the following command for
cloning Resumi:

```sh
git clone --depth 1 https://framagit.org/severo/resumi.git themes/resumi
```

#### Install Resumi as submodule

This method doesn't store a copy of Resumi's repository inside your site's
repository. Rather it adds Resumi as a dependency. Start by this command:

```sh
git submodule add https://framagit.org/severo/resumi.git themes/resumi
```

This will add Resumi's repository as a submodule to your site's repository. Now,
you will have to pull the theme:

```sh
git submodule init
git submodule update
```

That's all, Resumi is ready to be used.

## Configuration for Resumi

For getting started with Resumi, copy the **`config.yl`** file from the
**`exampleSite`** directory inside Resumi's repository to your site repository:

```sh
cp themes/resumi/exampleSite/config.yml .
```

You can take a look at the [**`config.yml`** file](../exampleSite/config.yml) of this site.

Now, you can start editing this file and change the configuration!

### Setting up Widgets

If you want to use Widgets on your site, take a look at the [Widgets
documentation](./widgets.md)

---

Et voilà! Resumi is ready!
