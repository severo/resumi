# Search: Fuse.js

## Configure Fuse.js Search Client

Select Fuse.js as the search client in your `config.yml` file:

```yaml
params:
  search:
    client: 'fuse'
```
