# Translation Support

You can translate Resumi in your own language!

For example, if you want to translate Resumi in Spanish, you will have to add
this to your site's config file:

```yaml
languages:
  es:
    lang: 'es'
    languageName: 'Spanish'
    weight: 1
```

Then create a folder named **`/i18n/`** in your site's root. And create a file
**`/i18n/es.toml`** with the translated strings.

For reference template you can see the
[en.toml](../i18n/en.toml) file.

If you want your translation file to be included in Resumi repository, feel free
to create a merge request.
